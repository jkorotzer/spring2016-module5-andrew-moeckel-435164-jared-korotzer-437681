<?php
    require 'database.php';
    ini_set("session.cookie_httponly", 1);
    
    session_start();
    $previous_ua = @$_SESSION['user_agent'];
    $current_ua = $_SERVER['HTTP_USER_AGENT'];
 
    if(isset($_SESSION['user_agent']) && $previous_ua !== $current_ua){
        die("Session hijack detected");
    }else{
        $_SESSION['user_agent'] = $current_ua;
    }
    if($_POST['token'] == $_SESSION['token']) {
        if(isset($_POST['day']) AND isset($_POST['month']) AND isset($_POST['year']) AND isset($_POST['description']) ) {
            $day = mysqli_real_escape_string($mysqli, $_POST['day']);
            $month = mysqli_real_escape_string($mysqli, $_POST['month']);
            $year = mysqli_real_escape_string($mysqli, $_POST['year']);
            $description = mysqli_real_escape_string($mysqli, $_POST['description']);
            $time = mysqli_real_escape_string($mysqli, $_POST['time']);
            $category = mysqli_real_escape_string($mysqli, $_POST['category']);
            $userID = $_SESSION['id'];
            if(isset($_POST['shareUser'])) {
                $share = $_POST['shareUser'];
                $stmt = $mysqli->prepare("SELECT id FROM users WHERE (username = ?)");
                $stmt->bind_param('s', $share);
                $stmt->execute();

                $stmt->bind_result($shareID);
                $stmt->fetch();
                $stmt->close();
                $stmt = $mysqli -> prepare("INSERT INTO events(day, month, year, description, time, category, user_id) VALUES(?, ?, ?, ?, ?, ?, ?)");
                if(!$stmt) {
                    printf("Query failed");
                    exit;
                }
                $stmt -> bind_param('iiisssi', $day, $month, $year, $description, $time, $category, $shareID);
                $stmt -> execute();
                $stmt -> close();
                echo json_encode(array(
                   "post" => true
                ));
            } else {
                $stmt = $mysqli -> prepare("INSERT INTO events(day, month, year, description, time, category, user_id) VALUES(?, ?, ?, ?, ?, ?, ?)");
                if(!$stmt) {
                    printf("Query failed");
                    exit;
                }
                $stmt -> bind_param('iiisssi', $day, $month, $year, $description, $time, $category, $userID);
                $stmt -> execute();
                $stmt -> close();
                echo json_encode(array(
                   "post" => true
                ));
            }    
        }
    }
?>

