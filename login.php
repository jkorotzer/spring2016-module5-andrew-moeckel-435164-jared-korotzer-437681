<?php
    require "database.php";
    if(isset($_POST['username']) AND isset($_POST['password'])) {
        $_SESSION['loggedIn']=false;
        $username = $_POST["username"];
        $password = $_POST["password"];
        $stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM users WHERE username=?");
        // Bind the parameter
        $stmt->bind_param('s', $username);
        $stmt->execute();
        // Bind the results
        $stmt->bind_result($cnt, $id, $pwd_hash);
        $stmt->fetch();
        // Compare the submitted password to the actual password hash
        if( $cnt == 1 && crypt($password, $pwd_hash)==$pwd_hash){
            ini_set("session.cookie_httponly", 1);
            session_start();
            $_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            $_SESSION['user'] = $username;
            $_SESSION['id'] = $id;
            $_SESSION['loggedIn'] = true;
            $_SESSION['token'] = substr(md5(rand()), 0, 10);
            $token = $_SESSION['token'];
            echo json_encode(array(
               "login" => true,
               "userid" => htmlentities($id),
               "token" => htmlentities($token)
            ));
        }
        else {
            echo json_encode(array(
                "login" => false,
                "error" => "Incorrect username or password."
            ));
        }
    }
?>