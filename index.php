<!DOCTYPE html>
<html>
<head>
	<title>Calendar</title>
    <link rel="stylesheet" type="text/css" href="index.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
    	<div class="container">
            <a class="navbar-brand">The Amazing Calendar</a>
    		<div class="collapse navbar-collapse form-group">
                <div id="block">
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-success" id="login" name="login" value="Login" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-success" id="signup" name="signup" value="Sign Up"/>
                    </div>
                </form>
                </div>
    		</div>
    	</div>
	</div>
    
    <div id="dateDisplay">
        <input type="submit" id="previous" value="Previous"/>
        <div id="date"></div>
        <input type="submit" id="next" value="Next"/>
    </div>
    <table id="calendarTable" class="calendar">
        <tr class="weekdays">
            <td>Sunday</td>
            <td>Monday</td>
            <td>Tuesday</td>
            <td>Wednesday</td>
            <td>Thursday</td>
            <td>Friday</td>
            <td>Saturday</td>
        </tr>
        <tr class="week0">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
        <tr class="week1">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
        <tr class="week2">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
        <tr class="week3">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
        <tr class="week4">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
        <tr class="week5">
            <td class="day0"></td>
            <td class="day1"></td>
            <td class="day2"></td>
            <td class="day3"></td>
            <td class="day4"></td>
            <td class="day5"></td>
            <td class="day6"></td>
        </tr>
    </table>
    
    <div id="post" class="jumbotron">
        <form>
            <div class="form-group">
                <label for="month">Month</label>
                <input type="number" class="form-control" name="month" id="month" min="1" max="12"/>
            </div>
            <div class="form-group">
                <label for="day">Day</label>
                <input type="number" class="form-control" name="day" id="day" min="1" max="31"/>
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" class="form-control" name="year" id="year" min="0"/>
            </div>
            <div class="form-group">
                <label for="time">Time</label>
                <input type="time" class="form-control" name="time" id="time" />
            </div>
			<div class="form-group">
                <label for="share">Share with user:</label>
                <input type="text" class="form-control" name="share" id="share" placeholder="Leave blank if you do not wish to share this event with another user." />
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" class="form-control" id="description"></textarea>
            </div>
			Category:
			<label class="radio-inline"><input type="radio" name="category" value="school" id="school">School</label>
			<label class="radio-inline"><input type="radio" name="category" value="birthday" id="birthday">Birthday</label>
			<label class="radio-inline"><input type="radio" name="category" value="hobby" id="hobby">Hobby</label>
			<label class="radio-inline"><input type="radio" name="category" value="work" id="work">Work</label><br/><br/>
            <button class="btn btn-success" id="poster" name="submit">Post</button>
            <button id='edit' class='btn btn-success'>Edit</button>
            <button id='delete' class='btn btn-danger'>Delete</button>
        </form>
    </div>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="calendarHelper.js"></script>
    <script src="loginAndSignUp.js"></script>
    <script src="updateCalendar.js"></script>
    <script type="text/javascript">
        
        var currentMonth;
        var currentYear;
        var loggedIn;
        var currentEvent;
		var token;
        
        function nextMonth() {
            currentMonth++;
            if (currentMonth == 12) {
                currentMonth = 0;
                currentYear++;
            }
            updateCalendar(currentYear, currentMonth);
        }
        
        function previousMonth() {
            currentMonth--;
            if (currentMonth == -1) {
                currentMonth = 11;
                currentYear--;
            }
            updateCalendar(currentYear, currentMonth);
        }
        
        $(document).on("click", "td", function() {
            var date = $(this).text().split("/");
            if (loggedIn) {
                updateEvent(date[0], date[1], date[2]);
            }
        });
        
        $(document).on("click", "#delete", function(event) {
            event.preventDefault();
            var day = document.getElementById("day").value;
            var month = document.getElementById("month").value;
            var year = document.getElementById("year").value;
            var time = document.getElementById("time").value;
            var description = document.getElementById("description").value;
            var postData = "day="+day+"&month="+month+"&year="+year+"&description="+description+"&time="+time+"&token="+token;
			var deleteData = "id=" + currentEvent+"&token="+token;
            console.log(currentEvent);
            $.ajax({
                url: "delete.php",
                type: "POST",
                data: deleteData,
                success: function(data, textStatus, jqXHR) {
                    clearForm();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
        
        $(document).on("click", "#edit", function(event) {
            event.preventDefault();
            var day = document.getElementById("day").value;
            var month = document.getElementById("month").value;
            var year = document.getElementById("year").value;
            var time = document.getElementById("time").value;
            var description = document.getElementById("description").value;
			var category = $('input[name="category"]:checked').val();
			var share = document.getElementById("share").value;
            var postData = "day="+day+"&month="+month+"&year="+year+"&description="+description+"&time="+time+"&id="+currentEvent+"&category="+category+"&token="+token;
            var deleteData = "id="+currentEvent+"&token="+token;
            $.ajax({
                url: "delete.php",
                type: "POST",
                data: deleteData,
                success: function(data, textStatus, jqXHR) {
                    clearForm();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
            $.ajax({
                url: "post.php",
                type: "POST",
                //contentType: "application/json",
                data: postData,
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    if (json.post) {
                        //alert("success");
                        clearForm();
                    } else {
						console.log("error");
                        //alert("nope");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
					console.log("error");
                    //alert("error");
                }
            });
			if (share.length != 0) {
				postData = postData + "&shareUser="+share;
				$.ajax({
					url: "post.php",
					type: "POST",
					data: postData,
					success: function(data, textStatus, jqXHR) {
						var json = JSON.parse(data);
						if (json.post) {
							clearForm();
						} else {
							//alert("nope");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						//alert("error");
					}
				});
			}
        });
        
        $(document).ready(function() {
            loggedIn = false;
            //console.log(loggedIn);
            $("#post").hide();
            $("#eventDisplay").hide();
            document.getElementById("signup").addEventListener("click", signup, false);
            document.getElementById("login").addEventListener("click", login, false);
            document.getElementById("poster").addEventListener("click", post, false);
            var today = new Date();
            currentMonth = today.getMonth();
            console.log(currentMonth);
            currentYear = today.getFullYear();
            updateCalendar(currentYear, currentMonth);
            document.getElementById("next").addEventListener("click", nextMonth, false);
            document.getElementById("previous").addEventListener("click", previousMonth, false);
        });
        
        
    </script>    
</body>
</html>