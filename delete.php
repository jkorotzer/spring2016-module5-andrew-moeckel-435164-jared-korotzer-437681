<?php

include_once('database.php');
header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);

session_start();
$previous_ua = @$_SESSION['user_agent'];
$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
if(isset($_SESSION['user_agent']) && $previous_ua !== $current_ua){
	die("Session hijack detected");
}else{
	$_SESSION['user_agent'] = $current_ua;
}
    if($_POST['token'] == $_SESSION['token']) {
        $id = $_POST['id'];
        $stmt = $mysqli->prepare("DELETE FROM events WHERE id=?");
        // Bind the parameter
        $stmt->bind_param('d', $id);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array(
            'success' => true                
        ));
    }
?>