<?php
    require "database.php";
    ini_set("session.cookie_httponly", 1);
    session_start();
    $previous_ua = @$_SESSION['user_agent'];
    $current_ua = $_SERVER['HTTP_USER_AGENT'];
 
    if(isset($_SESSION['user_agent']) && $previous_ua !== $current_ua){
        die("Session hijack detected");
    }else{
        $_SESSION['user_agent'] = $current_ua;
    }
    if($_POST['token'] == $_SESSION['token']) {
        if(isset($_POST['day']) AND isset($_POST['month']) AND isset($_POST['year']) AND isset($_SESSION["id"])) {
            
            $day = $_POST["day"];
            $month = $_POST["month"];
            $year = $_POST["year"];
            $id = $_SESSION["id"];
            $stmt = $mysqli->prepare("SELECT count(*), description, id, time, category FROM events WHERE (day=? AND month=? AND year=? AND user_id=?)");
            // Bind the parameter
            $stmt->bind_param('dddd', $day, $month, $year, $id);
            $stmt->execute();
            // Bind the results
            $stmt->bind_result($count, $description, $id, $time, $category);
            $stmt->fetch();
            $stmt->close();
            if($count > 0) {
                echo json_encode(array(
                    "hasDescription" => true,
                    "description" => htmlentities($description),
                    "day" => htmlentities($day),
                    "month" => htmlentities($month),
                    "year" => htmlentities($year),
                    "time" => htmlentities($time),
                    "category" => htmlentities($category),
                    "id" => htmlentities($id)
                ));
            } else {
                echo json_encode(array(
                   "hasDescription" => false                    
                ));
            }
        }
    }
?>