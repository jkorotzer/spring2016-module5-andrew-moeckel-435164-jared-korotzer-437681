function updateCalendar(year, month) {
    nowMonth = new Month(year, month);
    $("#date").html(monthToString(month) + " " + year);
    for(var i = 0; i < 7; i++) {
        $(".week5").find(".day" + i).html("");
    }
    var weeks = nowMonth.getWeeks();
    for(var c=0; c<weeks.length; c++){
        var w = weeks[c];
        var days = w.getDates();
        for(var k=0; k< days.length; k++) {
            var d = days[k];
            if (!(c === 0 && d.getDate() > 7) && !(c == 5 && d.getDate() < 10) && !(c == 4 && d.getDate() < 10)) {
                /*if (loggedIn) {
                    var data = "day="+d.getDate()+"&month="+ (month) + "&year="+year+"&token="+token;
                    $.ajax({
                        url: "fetchEvent.php",
                        type: "POST",
                        data: data,
                        success: function(data, textStatus, jqXHR) {
                            console.log("success");
                            var json = JSON.parse(data);
                            console.log(json);
                            if (json.hasDescription) {
                                console.log('description');
                                //$(".week" + c).find(".day" + k).html((month+1) +"/"+d.getDate()+"/"+year+"Has Event");
                                $(".week" + c).find(".day" + k).html("HI");
                            } else {
                                console.log('none');
                                $(".week" + c).find(".day" + k).html((month+1) +"/"+d.getDate()+"/"+year);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("error");
                        }
                    });
                } else {*/
                    $(".week" + c).find(".day" + k).html((month+1) +"/"+d.getDate()+"/"+year);
                //}
            } else {
                $(".week" + c).find(".day" + k).html("");
            }
        }
    }
}

function updateEvent(month, day, year) {
    console.log(month + "/" + day + "/" + year);
    var data = "day="+day+"&month="+month + "&year="+year+"&token="+token;
    $.ajax({
        url: "fetchEvent.php",
        type: "POST",
        data: data,
        success: function(data, textStatus, jqXHR) {
            var json = JSON.parse(data);
            if (json.hasDescription) {
                currentEvent = json.id;
                $('input[name="category"]').prop('checked', false);
                $("#day").val(json.day);
                $("#month").val(json.month);
                $("#year").val(json.year);
                $("#description").val(json.description);
                $('#time').val(json.time);
                if (json.category) {
                    $("#"+json.category).prop("checked", true);
                }
                //$("#postButtons").html("<button id='edit' class='btn btn-success'>Edit</button> <button id='delete' class='btn btn-danger'>Delete</button>");
                //$("#eventDisplay").show();
            } else {
                //$("#eventDisplay").hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
           // alert("error");
        }
    });
}
