function login(event) {
            event.preventDefault();
            var username = document.getElementById("username").value;
            var password = document.getElementById("password").value;
            var data = "username="+username+"&password="+password;
            $.ajax({
                url: "login.php",
                type: "POST",
                data: data,
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    if (json.login) {
                        loggedIn = true;
                        token = json.token;
                        updateCalendar(currentYear, currentMonth);
                        console.log(json.userid);
                        $("#block").hide();
                        setTimeout(function() {
                            $("#post").fadeIn();
                        }, 500);
                    } else {
                        alert(json.error);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
           
        }
        
function signup(event) {
    event.preventDefault();
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var data = "username="+username+"&password="+password;
    $.ajax({
        url: "signup.php",
        type: "POST",
        //contentType: "application/json",
        data: data,
        success: function(data, textStatus, jqXHR) {
            var json = JSON.parse(data);
            if (json.signup) {
                alert("success");
                
            } else {
                alert("nope");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    }); 
}
        
function post(event) {
    event.preventDefault();
    var day = document.getElementById("day").value;
    var month = document.getElementById("month").value;
    var year = document.getElementById("year").value;
    var time = document.getElementById("time").value;
    var description = document.getElementById("description").value;
    var share = document.getElementById("share").value;
    var category = $('input[name="category"]:checked').val();
    var data = "day="+day+"&month="+month+"&year="+year+"&description="+description+"&time="+time+"&category="+category+"&token="+token;
    $.ajax({
        url: "post.php",
        type: "POST",
        //contentType: "application/json",
        data: data,
        success: function(data, textStatus, jqXHR) {
            var json = JSON.parse(data);
            if (json.post) {
                //alert("success");
                clearForm();
            } else {
                //alert("nope");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //alert("error");
        }
    });
    if (share.length !== 0) {
        data = data + "&shareUser="+share;
        $.ajax({
        url: "post.php",
        type: "POST",
        //contentType: "application/json",
        data: data,
        success: function(data, textStatus, jqXHR) {
            var json = JSON.parse(data);
            if (json.post) {
                //alert("success");
                clearForm();
            } else {
                //alert("nope");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //alert("error");
        }
    });
    }
}

function clearForm() {
    $("#day").val("");
    $("#month").val("");
    $("#year").val("");
    $("#time").val("");
    $("#description").val("");
    $("#share").val("");
    $('input[name="category"]').prop('checked', false);
}